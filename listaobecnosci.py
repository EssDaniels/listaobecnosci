import os
from subprocess import Popen

from docxtpl import DocxTemplate
from openpyxl import load_workbook

LIBRE_OFFICE = r"C:\Program Files\LibreOffice\program\soffice.exe"

lista = []


def listaobecnosci():
    workbook = load_workbook(filename="notes_data.xlsx")
    lista = workbook
    print(lista)
    sheet = workbook.active
    print(sheet)

    a = 4
    b = 5
    c = 6
    d = str(sheet["H2"].value)

    if d == 'None':
        print("Brak danych. Proszę poprawić ilość pracowników w danej grupie")
    else:
        f = int(d) / 3
    t = 1

    lista_wybor = str(input("""Proszę wybrać: 
    1. Lista obecności  
    2. Załącznik nr1 
    3. Podsumowanie 
    """))

    if lista_wybor == "1":

        zmiana = str(input("Proszę o podanie która to zmiana: "))

        for _ in range(0, int(f) + 1):
            doc = DocxTemplate('note6-template.docx')

            context = {
                'gr': zmiana,
                'nr': t,
                'miesiac': "grudzień", #str(sheet["J2"].value),
                'pracownik1': str(sheet["C" + str(a)].value),
                'pracownik2': str(sheet["C" + str(b)].value),
                'pracownik3': str(sheet["C" + str(c)].value)
            }
            s = str(t)
            nazwapliku = "lista_obecnosci" + zmiana + s + ".docx"
            doc.render(context)

            doc.save(nazwapliku)

            def convert_to_pdf(input_docx, out_folder):
                p = Popen([LIBRE_OFFICE, '--headless', '--convert-to', 'pdf', '--outdir',
                           out_folder, input_docx])
                print([LIBRE_OFFICE, '--convert-to', 'pdf', input_docx])
                p.communicate()

            sample_doc = nazwapliku
            out_folder = 'Zmiana ' + zmiana
            convert_to_pdf(sample_doc, out_folder)
            os.remove(nazwapliku)

            a = c + 1
            b = a + 1
            c = b + 1
            t += 1

    elif lista_wybor == "2":

        for _ in range(0, int(d)):
            doc = DocxTemplate('Zal1.docx')

            context = {
                'miesiac': "grudzień",
                'pracownik': str(sheet["C" + str(a)].value),
            }
            s = str(t)
            nazwapliku = "Zalacznik" + s + ".docx"
            doc.render(context)

            doc.save(nazwapliku)

            def convert_to_pdf(input_docx, out_folder):
                p = Popen([LIBRE_OFFICE, '--headless', '--convert-to', 'pdf', '--outdir',
                           out_folder, input_docx])
                print([LIBRE_OFFICE, '--convert-to', 'pdf', input_docx])
                p.communicate()

            sample_doc = nazwapliku
            out_folder = 'Zalacznik1 '
            convert_to_pdf(sample_doc, out_folder)
            os.remove(nazwapliku)

            a += 1
            t += 1

    elif lista_wybor == "3":



        for _ in range(0, int(f) + 1):
            doc = DocxTemplate('podsumowanie.docx')

            context = {

                'pracownik1': str(sheet["C" + str(a)].value),
                'pracownik2': str(sheet["C" + str(b)].value),
                'pracownik3': str(sheet["C" + str(c)].value)
            }
            s = str(t)
            nazwapliku = "podsumowanie" + s + ".docx"
            doc.render(context)

            doc.save(nazwapliku)

            def convert_to_pdf(input_docx, out_folder):
                p = Popen([LIBRE_OFFICE, '--headless', '--convert-to', 'pdf', '--outdir',
                           out_folder, input_docx])
                print([LIBRE_OFFICE, '--convert-to', 'pdf', input_docx])
                p.communicate()

            sample_doc = nazwapliku
            out_folder = 'Podsumowanie'
            convert_to_pdf(sample_doc, out_folder)
            os.remove(nazwapliku)

            a = c + 1
            b = a + 1
            c = b + 1
            t += 1



listaobecnosci()
