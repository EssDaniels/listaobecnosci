from docxtpl import DocxTemplate
import jinja2
import pandas as pd
from openpyxl import load_workbook
import os
import tkinter as tk # ładowanie modułu tkinter


workbook = load_workbook(filename="notes_data.xlsx")
sheet = workbook.active
i = 0
a = 4
b = a + 1
c = b + 1
d = str(sheet["H2"].value)
f = int(d) / 3
t = 1
zmiana = 'II'

for i in range(0, int(f) + 1):
    doc = DocxTemplate('note6-template.docx')

    context = {
        'gr': zmiana,
        'nr': t,
        'miesiac': str(sheet["J2"].value),
        'pracownik1': str(sheet["C"+str(a)].value),
        'pracownik2': str(sheet["C"+str(b)].value),
        'pracownik3': str(sheet["C"+str(c)].value)
    }
    s = str(t)
    nazwapliku = "lista_obecnosci" + zmiana + s + ".docx"
    doc.render(context)

    doc.save(nazwapliku)



    a = c + 1
    b = a + 1
    c = b + 1
    t = t + 1


